<?php
// Turn on the display of all errors.
ini_set('display_errors', 1);
require_once('../vendor/autoload.php');

// Define the path to your root folder. 1 folder higher than this.
define('APP_BASE', realpath(dirname(__FILE__) . '/../'));

$config = require APP_BASE . '/config/config.php';

$app = new \Slim\Slim(
    $config['slim']
);

require_once(APP_BASE . '/app/routes/routes-index.php');

$app->run();