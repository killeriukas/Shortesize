<?php
namespace Shortesize;

class Database
{
    private $databaseCon = null;

    /**
     * @var \MongoCollection
     */
    private $collection = null;

    /**
     * @param $database - database name
     * @param $collection - collection name
     * @return boolean false - if connection to database has failed
     * @throws \Exception - if parameters are wrong
     */
    public function Connect($database, $collection)
    {
        if (!is_string($database)) {
            throw new \Exception("Passed database name is not a string! Database: " . $database);
        }

        if (!is_string($collection)) {
            throw new \Exception("Passed collection name is not a string! Collection: " . $collection);
        }

        $this->databaseCon = new \MongoClient();

        if (!$this->databaseCon->connected) {
            return false;
        }

        $this->collection = $this->databaseCon->selectCollection($database, $collection);

        if ($this->collection == null) {
            return false;
        }
    }

    public function AddRecord($recordInfo, $index)
    {
        if (!is_array($recordInfo)) {
            return false;
        }

        if (!is_string($index)) {
            return false;
        }

        if ($this->collection == null) {
            return false;
        }


        if (false !== self::FindOne($recordInfo)) {
            return false;
        }

        $this->collection->ensureIndex(array($index => 1), array('unique' => true));
        $this->collection->insert($recordInfo);

        return true;
    }

    public function FindOne($query)
    {
        if ($this->collection == null) {
            return false;
        }

        if (!is_array($query)) {
            return false;
        }

        $record = $this->collection->findOne($query);

        if (null === $record) {
            return false;
        }

        return $record;
    }

}