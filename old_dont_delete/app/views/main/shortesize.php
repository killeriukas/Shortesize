<?php
/* @var $this \Slim\View */
$this->display('partials/header.php');

if (false !== $short) {
?>
    <h2>Short URL: <?php echo $short; ?></h2>
<?php
}

if (true === $invalid) {
?>
    <div class="alert alert-danger">
        <p><?php echo $errorMsg; ?></p>
    </div>
<?php
}
?>

<!--<div class="container">-->
    <form role="form" action="" method="post">
        <div class="form-group">
            <label for="longWebsiteUrl">Long URL</label>
            <input type="text" class="form-control" id="longWebsiteUrl" placeholder="Copy URL here" name="longUrl">
        </div>
        <div class="align-center margin-bottom">
            <button type="submit" class="btn btn-default" name="makeShort">Short</button>
        </div>
    </form>
<!--</div>-->

<?php
//if(isset($prefix) && strlen($prefix[0]) === 6)
//{
//    $db = new MongoClient();
//    $coll = $db->selectCollection('tmi', 'shortesize');
//
//    $record = $coll->findOne(array('shortUrl' => $shortUrl));
//
//    //record doesn't exist
//    if ($record == null) {
//        //change this one to the proper session object
//        $_SESSION['success'] = false;
//        $_SESSION['message'] = 'Unfortunately, your requested URL doesn\'t exist.';
//    } else { //record exists
//        Helper::Redirect($record['longUrl'], true);
//        exit;
//    }
//
//}

$this->display('partials/footer.php');