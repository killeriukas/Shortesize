<?php
/* @var $app \Slim\Slim */

$app->get(
    '/',
    function () use ($app) {
        $session = \OhSession\Session::getInstance();
        $sessionVar = $session->get('shortUrl');
        if ($sessionVar) {
            $sessionVar = $app->request()->getScheme() . '://' . $app->request()->getHostWithPort() . '/' . $sessionVar;
            $session->remove('shortUrl');
        }
        $sessionInvalid = $session->get('invalid');
        $session->remove('invalid');
        $sessionError = $session->get('errorMsg');
        $session->remove('errorMsg');
        $app->render(
            'main/shortesize.php',
            array(
                'short' => $sessionVar,
                'invalid' => $sessionInvalid,
                'errorMsg' => $sessionError,
                'common' => array(
                    'title' => 'Shortesize URL'
                )
            )
        );
    }
);

// Haven't touched this method.
$app->get(
    '/:prefix',
    function ($prefix) use ($app) {

        $globalConfig = require APP_BASE . '/config/config.php';
        $config = $globalConfig[$globalConfig['slim']['mode']];

        $db = new \Shortesize\Database();
        $db->Connect($config['db']['name'], $config['db']['collection']);

        $record = $db->FindOne(array('shortUrl' => $prefix));

        if(false === $record)
        {
            $session = \OhSession\Session::getInstance();
            $session->set('invalid', true);
            $session->set('errorMsg', 'The URL doesn\'t exist.');
            $app->redirect('/');
            return;
        }

        $app->redirect($record['longUrl']);

//        $app->render('main/prefixed.php',
//            array(
//                'prefix' => $prefix,
//                'common' => array(
//                    'title' => 'Searching URL'
//                )
//            )
//        );
    }
);

$app->post(
    '/',
    function () use ($app) {

        $postedUrl = $app->request->post('longUrl');

        $session = \OhSession\Session::getInstance();

        if (!filter_var($postedUrl, FILTER_VALIDATE_URL)) {
            $session->set('invalid', true);
            $session->set('errorMsg', 'The URL you have entered is invalid.');
            $app->redirect('/');
            return;
        }

        $shortUrl = \Shortesize\Converter::getShortcut(6);

        $globalConfig = require APP_BASE . '/config/config.php';
        $config = $globalConfig[$globalConfig['slim']['mode']];

        $db = new \Shortesize\Database();
        $db->Connect($config['db']['name'], $config['db']['collection']);

        $succ = $db->AddRecord(array('shortUrl' => $shortUrl, 'longUrl' => $postedUrl, 'time' => new MongoDate(), 'ip' => $app->request->getIp()), 'shortUrl');

        if(false === $succ)
        {
            $session->set('invalid', true);
            $session->set('errorMsg', 'Error from the database. (Under maintenance)');
            $app->redirect('/');
            return;
        }

        $session->set('shortUrl', $shortUrl);
        $app->redirect('/');
    }
);