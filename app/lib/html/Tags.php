<?php

namespace lib\html;

use lib\data_structure\Dictionary;
use lib\html\form\AForm;

final class Tags
{


    const ATTRIBUTES = '%ATTRIBUTES%';

    const FORM_ACTION_NAME = 'FORM_ACTION_NAME';

//    public function AddText()
//    {
//        echo '<input type="text">';
//    }
//
//    public function AddLabel()
//    {
//        echo '<label></label>';
//    }
//
//    public function AddHeader()
//    {
//        echo '<h1></h1>';
//    }

    public static function CreateSpan($title, HtmlAttributes $attributes = null)
    {
        return self::Filter('<span ' . self::ATTRIBUTES . '>' . $title . '</span>', $attributes);
    }

//    public function AddLink($title, $link, HtmlAttributes $attributes = null)
//    {
//        echo $this->CreateLink($title, $link, $attributes);
//    }

    public static function Filter($text, HtmlAttributes $attributes = null)
    {
        if($attributes === null) {
            return str_replace(self::ATTRIBUTES, '', $text);
        }
        return str_replace(self::ATTRIBUTES, $attributes->GetAllAttributes(), $text);
    }

    public static function CreateLink($title, $link, HtmlAttributes $attributes = null)
    {
        return self::Filter('<a href="' . $link . '"' . self::ATTRIBUTES . '>' . $title . '</a>', $attributes);
    }

    public static function StartDiv(HtmlAttributes $attributes = null)
    {
        return self::Filter('<div ' . self::ATTRIBUTES . '>', $attributes);
    }

    public static function EndDiv()
    {
        return '</div>';
    }

    public function CreatePostButton($url, $title, $postType, $class = null)
    {
        $postButton = $this->StartForm(true, $url);
        $postButton .= $this->CreateInputHidden(new HtmlAttributes(new Dictionary(array('value' => $postType))));

        $submitButtonDesign = new Dictionary();
        $submitButtonDesign->Add('name', self::FORM_ACTION_NAME);
        $submitButtonDesign->Add('class', $class);

        $postButton .= $this->CreateButtonSubmit($title, new HtmlAttributes($submitButtonDesign));
        $postButton .= $this->EndForm();

        return $postButton;
    }

    public function StartForm($method, $action, HtmlAttributes $attributes = null)
    {
        switch($method) {
            case AForm::FORM_METHOD_POST:
            case AForm::FORM_METHOD_GET:
                return self::Filter('<form method="' . $method . '" action="' . $action . '"' . self::ATTRIBUTES . '>', $attributes);
            case AForm::FORM_METHOD_DELETE:
            case AForm::FORM_METHOD_PUT:
                $form = self::Filter('<form method="' . $method . '" action="' . $action . '"' . self::ATTRIBUTES . '>', $attributes);
                $form .= $this->CreateInputHidden(new HtmlAttributes(new Dictionary(array('name' => self::FORM_ACTION_NAME, 'value' => $action))));
                return $form;
            default:
                throw new \InvalidArgumentException("Passed method doesn't exist in the form: " . $method);
        }
    }

    public function EndForm()
    {
        return '</form>';
    }

    public function CreateInputHidden(HtmlAttributes $attributes = null)
    {
        return self::CreateInputField('hidden', $attributes);
    }

    public function CreateButtonSubmit($title, HtmlAttributes $attributes = null)
    {
        return self::Filter('<button type="submit"' . self::ATTRIBUTES . '>' . $title . '</button>', $attributes);
    }

    public function CreateInputSubmit(HtmlAttributes $attributes = null)
    {
        return self::CreateInputField('submit', $attributes);
    }

    private static function CreateInputField($type, HtmlAttributes $attributes)
    {
        return self::Filter('<input type="' . $type . '" ' . self::ATTRIBUTES . '>', $attributes);
    }

    private static function CreateButton($type, $title, HtmlAttributes $attributes)
    {
        return self::Filter('<button type="' . $type . '" ' . self::ATTRIBUTES . '>' . $title . '</button>', $attributes);
    }

    public static function CreateButtonDefault($title, HtmlAttributes $attributes = null)
    {
        return self::CreateButton('button', $title, $attributes);
    }

    public function AddButtonSubmit()
    {
        echo '<input type="submit">';
    }


}