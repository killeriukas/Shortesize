<?php

namespace lib\html\validator;

use lib\data_structure\Dictionary;

class AlphaNumeric extends ValidatorBase
{

    public function IsValid($value, Dictionary $valueList)
    {
        $isValid = ctype_alnum($value);

        if(!$isValid)
        {
            $this->AddMessage('This field must contain only numbers and letters.');
            return false;
        }

        return true;
    }
}