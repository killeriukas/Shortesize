<?php

namespace lib\html\validator;

use lib\data_structure\Dictionary;

final class ValidatorNotEmpty extends ValidatorBase
{

    public function IsValid($value, Dictionary $values)
    {
        $isValid = !empty($value) && strlen($value) > 0;

        if(!$isValid)
        {
            $this->AddMessage('The field cannot be empty');
            return false;
        }

        return true;
    }
}