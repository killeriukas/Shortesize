<?php

namespace lib\html\validator;

use lib\data_structure\Dictionary;

class Email extends ValidatorBase
{

    public function IsValid($value, Dictionary $valueList)
    {
        $isValid = filter_var($value, FILTER_VALIDATE_EMAIL);

        if($isValid === false)
        {
            $this->AddMessage('E-mail is not valid! (1)');
            return false;
        }

        $sanitizedEmail = filter_var($value, FILTER_SANITIZE_EMAIL);

        if($value !== $sanitizedEmail)
        {
            $this->AddMessage('E-mail is not valid! (2)');
            return false;
        }

        return true;
    }

}