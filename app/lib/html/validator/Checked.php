<?php

namespace lib\html\validator;

use lib\data_structure\Dictionary;

class Checked extends ValidatorBase
{

    public function IsValid($value, Dictionary $valueList)
    {
        if(null === $value)
        {
            $this->AddMessage($this->GetUncheckMessage($value));
            return false;
        }

        return true;
    }

    protected function GetUncheckMessage($value)
    {
        return 'Current value [' . $value . '] is unchecked';
    }

}