<?php

namespace lib\html\filter;

final class FilterTrim implements FilterBase
{
    public function Filter($value)
    {
        return trim($value);
    }
}