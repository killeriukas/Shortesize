<?php

namespace lib\html;

abstract class Html {

    const HTML_5 = 'html';
    const HTML_401_STRICT = 'HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"';

    private $language;
    private $title;
    private $tags;

    public function __construct($title, $language = 'en') {
        if (is_string($title)) {
            $this->title = $title;
        }

        if (is_string($language)) {
            $this->language = $language;
        }

        $this->tags = new Tags();
    }

    final public function StartHtml($parameters = null, $htmlDocType = Html::HTML_5) {
        echo '<!DOCTYPE ' . $htmlDocType . '>';
        echo '<html lang="' . $this->language . '">';
        echo '<head>';
        $this->PrintMetas();
        $this->PrintLinks();
        $this->PrintTopScripts();
        echo '<title>' . $this->title . '</title>';
        echo '</head>';
        echo '<body>';
        $this->PrintTopBody($parameters);
    }

    final public function GetTags()
    {
        return $this->tags;
    }

    protected abstract function PrintMetas();
    protected abstract function PrintTopScripts();
    protected abstract function PrintLinks();

    protected abstract function PrintTopBody($parameters);
    protected abstract function PrintBottomBody($parameters);
    protected abstract function PrintBottomScripts();

    final public function EndHtml($parameters = null) {
        $this->PrintBottomBody($parameters);
        $this->PrintBottomScripts();
        echo '</body>';
        echo '</html>';
    }

}
