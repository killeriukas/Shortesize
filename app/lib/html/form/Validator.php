<?php

namespace lib\html\form;

use lib\html\validator\ValidatorBase;

final class Validator
{

    private $fieldName;

    private $isRequired;

    private $validators;

    //TODO: not used for now
    private $options;

    public function __construct($fieldName, $isRequired, array $validators, array $options)
    {
        if(!is_string($fieldName))
        {
            throw new \Exception('Field name must be string!');
        }

        $this->fieldName = $fieldName;

        if(!is_bool($isRequired))
        {
            throw new \Exception('Is required must be a bool!');
        }

        $this->isRequired = $isRequired;

        if(empty($validators))
        {
            throw new \Exception('Validator cannot be without validators!');
        }

        foreach($validators as $validator)
        {
            if(!($validator instanceof ValidatorBase))
            {
                throw new \Exception('One of the validators is not from [ValidatorBase]');
            }
        }

        $this->validators = $validators;
        $this->options = $options;
    }

    public function GetFieldName()
    {
        return $this->fieldName;
    }

    public function IsRequired()
    {
        return $this->isRequired;
    }

    public function GetValidators()
    {
        return $this->validators;
    }
}