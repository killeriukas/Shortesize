<?php

namespace lib\shared;

final class Functions
{

    public static function GenerateGuid()
    {
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X',
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(16384, 20479),
            mt_rand(32768, 49151),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535)
        );
    }

    public static function CreateToken()
    {
        return sha1(microtime());
    }

    public static function FormatTimeSince($seconds)
    {
        $tokens = array(
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'min',
            1 => 'sec'
        );

        foreach($tokens as $unit => $text) {
            if($seconds < $unit) continue;
            $numberOfUnits = floor($seconds / $unit);
            return $numberOfUnits . ' ' . $text . ($numberOfUnits > 1 ? 's' : '') . ' ago';
        }
    }

    public static function FormatDateDefault($seconds)
    {
        return date('Y/m/d H:i:s', $seconds);
    }


    //deprecated below
    public static function EscapeHtml($sentence)
    {
        if(!self::IsNullOrEmpty($sentence)) {
            $escaped = htmlspecialchars($sentence);
            return $escaped;
        }
        return $sentence;
    }

}