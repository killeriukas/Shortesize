<?php

define('ACCEPTED_ENCODING', 'UTF-8');

define('PROJECT_NAME', 'Test Project');

define('DEVELOPMENT', 'development');
define('PRODUCTION', 'production');
define('APP_MODE', DEVELOPMENT);

return array(
    PRODUCTION => array(
        'mode' => PRODUCTION,
        'templates.path' => DEFAULT_APP_VIEWS,
        'db' => array(
            'server' => 'mongodb://localhost:27017',
            'options' => array(
                'connect' => true
            ),
            'dbName' => 'Not_Test_Database_0',
            'username' => 'admin_true',
            'password' => 'admin_0000_true'
        ),
        'debug' => false
    ),
    DEVELOPMENT => array(
        'mode' => DEVELOPMENT,
        'templates.path' => DEFAULT_APP_VIEWS,
        'db' => array(
            'server' => 'mongodb://localhost:27017',
            'options' => array(
                'connect' => true
            ),
            'dbName' => 'Test_Database_0',
            'username' => 'admin',
            'password' => 'admin_0000'
        ),
        'debug' => true
    )
);