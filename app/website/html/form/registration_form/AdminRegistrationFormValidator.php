<?php

namespace website\html\form\registration_form;

class AdminRegistrationFormValidator extends RegistrationFormValidator
{

    protected function CreateValidators()
    {
        $validators = parent::CreateValidators();

        return $validators;
    }

    protected function CreateFilters()
    {
        $filters = parent::CreateFilters();

        return $filters;
    }
}