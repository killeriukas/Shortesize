<?php

namespace website\html\account;

use website\html\GeneralHtml;

class GuestHtml extends GeneralHtml
{

    protected function PrintLinks()
    {
        parent::PrintLinks();
        ?>
        <link href="/css/account/general.css" rel="stylesheet">
    <?php
    }

    protected function PrintTopBody($parameters)
    {
        ?>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="nav navbar-header">
                    <a class="navbar-brand" href="/">Welcome, Traveler!</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="/faq">FAQ</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="/auth/login"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Log In</a>
                    </li>
                    <li>
                        <a href="/auth/register"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Register</a>
                    </li>
                </ul>
            </div>
        </nav>
    <?php
    }


}
