<?php

namespace website\html\panel\job_panel;

use lib\html\HtmlAttributes;
use website\html\panel\JobPanel;

final class GuestJobPanel extends JobPanel
{

    protected function PrintSaveButton(HtmlAttributes $attributes = null)
    {
        $guestAttributes = new HtmlAttributes();
        $guestAttributes->Add('class', 'disabled');
        parent::PrintSaveButton($guestAttributes);
    }

    protected function PrintApplyButton(HtmlAttributes $attributes = null)
    {
        $guestAttributes = new HtmlAttributes();
        $guestAttributes->Add('class', 'disabled');
        parent::PrintApplyButton($guestAttributes);
    }
}