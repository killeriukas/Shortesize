<?php

$permissions = require 'GuestPermissions.php';
$permissions[WEB_MY_ACCOUNT_GET] = true;
$permissions[WEB_LOGOUT_GET] = true;
$permissions[WEB_DB_GET] = true;
$permissions[WEB_COLLECTION_GET] = true;

return $permissions;