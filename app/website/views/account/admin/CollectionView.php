<?php
/* @var $user \website\models\users\UserModel */
$user = $data['user'];

/* @var $html \website\html\GeneralHtml */
$html = $data['html'];

/**
 * @var \lib\data_structure\GeneralList $userRecords
 */
$userRecords = $data['user_records'];

$html->StartHtml(array('user' => $user));
?>
    <div class="container-fluid">
        <h1>Collection &raquo; <?php echo $data['collection_id']; ?> (<?php echo $userRecords->Count(); ?>)</h1>
        <?php

        for ($i = 0; $i < $userRecords->Count(); ++$i) {
            $recordItem = $userRecords->Get($i);
            $recordTable = new \lib\html\table\Table(array('class' => 'table'));
            $recordTable->AddRow();
            $recordTable->AddHeader(array('# ' . ($i + 1), 'Parameter Name', 'Parameter Value'));
            foreach ($recordItem as $key => $value) {
                $recordTable->AddRow();
                $recordTable->AddColumn(array(''));
                $recordTable->AddHeader(array($key));
                $recordTable->AddColumn(array($value));
            }
            $recordTable->Render();
        }
        ?>
    </div>



<?php
$html->EndHtml();
