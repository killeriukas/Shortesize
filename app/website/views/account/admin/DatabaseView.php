<?php
/* @var $user \website\models\users\UserModel */
$user = $data['user'];

/* @var $html \website\html\GeneralHtml */
$html = $data['html'];

$html->StartHtml(array('user' => $user));
?>
    <div class="container-fluid">
        <h1>Database</h1>
    </div>
    <div class="container-fluid col-sm-3">
        <?php

        //general information
        use \lib\html\table\Table;
        $databaseConfig = $data['db_config'];
        $accountTable = new Table(array('class' => 'table'));
        $accountTable->AddRow();
        $accountTable->AddHeader(array('General Information'));
        $accountTable->AddColumn(array(''));
        $accountTable->AddRow();
        $accountTable->AddHeader(array('Server Host'));
        $accountTable->AddColumn(array($databaseConfig['server']));
        $accountTable->AddRow();
        $accountTable->AddHeader(array('Database Name'));
        $accountTable->AddColumn(array($databaseConfig['dbName']));

        /**
         * @var $databaseCollections \lib\data_structure\GeneralList
         */
        $databaseCollections = $data['db_collections'];
        $collectionCount = $databaseCollections->Count();
        $accountTable->AddRow();
        $accountTable->AddHeader(array('Collections Count'));
        $accountTable->AddColumn(array($collectionCount));

        $databaseRecords = $data['db_records'];
        $totalRecords = 0;

        /**
         * @var $record MongoCursor
         */
        foreach($databaseRecords as $record)
        {
            $totalRecords += $record->count();
        }

        $accountTable->AddRow();
        $accountTable->AddHeader(array('Records Count'));
        $accountTable->AddColumn(array($totalRecords));

        $accountTable->Render();
        ?>
    </div>
    <div class="container-fluid col-sm-2">
        <?php

        //collections information
        $collectionTable = new Table(array('class' => 'table'));
        $collectionTable->AddRow();
        $collectionTable->AddHeader(array('#', 'Collections', 'Count', 'Delete'));
        for($i = 0; $i < $collectionCount; ++$i)
        {
            $collectionTable->AddRow();
            $collectionTable->AddColumn(array($i));

            $collectionName = $databaseCollections->Get($i);

            $collectionTable->AddColumn(array($html->GetTags()->CreateLink($collectionName, '/db/collection/' . $collectionName . '/get')));
            $collectionTable->AddColumn(array($databaseRecords->Get($collectionName)->count()));

            $htmlDeleteSign = $html->GetTags()->CreatePostButton('/db/collections/',
                $html->GetTags()->CreateSpan('',
                    new \lib\html\HtmlAttributes(new \lib\data_structure\Dictionary(array('class' => 'glyphicon glyphicon-remove')))),
                \lib\html\Tags::FORM_ACTION_DELETE,
                'btn btn-xs btn-danger');

            $collectionTable->AddColumn(array($htmlDeleteSign));
        }
        $collectionTable->Render();
        ?>
    </div>
<?php
$html->EndHtml();
