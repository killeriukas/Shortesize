<?php

use website\html\account\MyAccountHtml;

$html = new MyAccountHtml('Document View');
$html->StartHtml();
?>
    <div class="container-fluid">
        <h1>Document View</h1>
        <div class="container">
            <?php

            use lib\html\table\Table;
            use lib\html\Message;

            if (isset($data['document'])) {

                /**
                 * @var $document \website\models\account\JobModel
                 */
                $document = $data['document'];

                $documentTable = new Table(array('class' => 'table table-bordered'));
                $documentTable->AddRow();
                $documentTable->AddHeader(array('Title'));
                $documentTable->AddColumn(array($document->GetTitle()));
                $documentTable->AddRow();
                $documentTable->AddHeader(array('Location'));
                $documentTable->AddColumn(array($document->GetLocation()));
                $documentTable->AddRow();
                $documentTable->AddHeader(array('Company'));
                $documentTable->AddColumn(array($document->GetCompany()));
                $documentTable->AddRow();
                $documentTable->AddHeader(array('Description'));
                $documentTable->AddColumn(array($document->GetDescription()));
                $documentTable->AddRow();
                $documentTable->AddHeader(array('Requirements'));
                $documentTable->AddColumn(array($document->GetRequirements()));
                $documentTable->AddRow();
                $documentTable->AddHeader(array('Salary, $'));
                $documentTable->AddColumn(array($document->GetSalary()));
                $documentTable->AddRow();
                $documentTable->AddHeader(array('People Applied'));
                $documentTable->AddColumn(array($document->GetPeopleApplied()));
                $documentTable->AddRow();
                $documentTable->AddHeader(array('Last Updated'));
                $documentTable->AddColumn(array($document->GetLastUpdated_Readable()));
                $documentTable->Render();
            } else {
                Message::Warning('Document was not found! <a href="/joblistings" class="alert-link">Wanna go back to the job listings?</a>');
            }
            ?>
        </div>
    </div>
<?php
$html->EndHtml();