<?php
/* @var $user \website\models\users\UserModel */
$user = $data['user'];

/* @var $html \website\html\GeneralHtml */
$html = $data['html'];

$html->StartHtml(array('user' => $user));
?>
<div class="container-fluid">
    <h1>My Account</h1>
</div>
<div class="container-fluid">
    <?php

    use lib\html\table\Table;
    use lib\html\HtmlAttributes;
    use lib\data_structure\Dictionary;
    use lib\data_structure\GeneralList;

    $accountTable = new Table(new HtmlAttributes(new Dictionary(array('class' => 'table'))));
    $accountTable->AddRow();
    $accountTable->AddHeader(new GeneralList(array('Account Information')), new HtmlAttributes(new Dictionary(array('class' => 'col-md-2'))));
    $accountTable->AddHeader(new GeneralList(array('test')));
    $accountTable->AddRow();
    $accountTable->AddHeader(new GeneralList(array('Account Information')), new HtmlAttributes(new Dictionary(array('class' => 'col-md-3'))));
    $accountTable->AddHeader(new GeneralList(array('test')));
//    $accountTable->AddColumn(array(''));
//    $accountTable->AddRow();
//    $accountTable->AddHeader(array('Unique Number'));
//    $accountTable->AddColumn(array($user->GetUniqueId()));
//    $accountTable->AddRow();
//    $accountTable->AddHeader(array('Account Type'));
//    $accountTable->AddColumn(array($user->GetAccountType()));
//    $accountTable->AddRow();
//    $accountTable->AddHeader(array('E-mail'));
//    $accountTable->AddColumn(array($user->GetEmail()));
//    $accountTable->AddRow();
//    $accountTable->AddHeader(array('User Name'));
//    $accountTable->AddColumn(array($user->GetUserName()));
//    $accountTable->AddRow();
//    $accountTable->AddHeader(array('Password'));
//    $accountTable->AddColumn(array('********'));
    $accountTable->Render();
    ?>
</div>
<!--<div class="container-fluid col-lg-2">-->
<!--    --><?php
//    $statisticsTable = new Table(array('class' => 'table'));
//    $statisticsTable->AddRow();
//    $statisticsTable->AddHeader(array('Statistics'));
//    $statisticsTable->AddColumn(array(''));
//    $statisticsTable->AddRow();
//    $statisticsTable->AddHeader(array('Information ...'));
//    $statisticsTable->AddColumn(array('Blah Blah'));
//
//    $statisticsTable->Render();
//    ?>
<!--</div>-->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<?php
$html->EndHtml();
