<?php

$html->StartHtml($data);
?>

<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <div class="page-header">
                <h1 class="pull-left">Menu</h1>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="page-header">
                <h1 class="pull-left">News</h1>
                <div class="pull-right" style="padding-top: 25px;">
                    <form class="form-inline">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="page-header">
                <h1 class="pull-left">Ads</h1>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-2">
test
    </div>
    <div class="col-lg-8">
        <?php

        /* @var $user \website\models\users\UserModel */
        $user = $data['user'];

        $savedJobs = $data['savedJobs'];

        /**
         * @var $jobModels \website\models\account\JobModel[];
         */
        $jobModels = $data['jobModels'];
        foreach ($jobModels as $jobModel) {
            echo '<div class="row">';

            $jobPanel = \website\html\panel\JobPanelFactory::Create($user->GetAccountType(), $jobModel);

            if(null !== $savedJobs) {
                $jobPanel->Update($savedJobs);
            }
            $jobPanel->Render();

            echo '</div>';
        }
        ?>
    </div>
    <div class="col-lg-2">
Ads test
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.ajax_save').click(function (e) {
            e.preventDefault();
            var curTarget = $(e.currentTarget).parents('div.panel');
            $(curTarget).removeClass('panel-default').addClass('panel-info');

            var jobId = $(e.currentTarget).attr('id');
            AddAjax('post',
                '/job/save',
                'job_id=' + jobId,
                function (info) {
                    console.log(info);
                },
                function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                });
        });
        $('.ajax_apply').click(function (e) {
            e.preventDefault();
            var curTarget = $(e.currentTarget).parents('div.panel');
            $(curTarget).removeClass('panel-default').addClass('panel-primary');

            var jobId = $(e.currentTarget).attr('id');
            AddAjax('post',
                '/job/apply',
                'job_id=' + jobId,
                function (info) {
                    console.log(info);
                },
                function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                });
        });
    });
</script>
<?php
$html->EndHtml();