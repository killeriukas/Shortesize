<?php

namespace website\models\users;

use lib\data_structure\Dictionary;

final class GuestUserModel extends UserModel
{

    public function __construct(Dictionary $parameters = null)
    {
        parent::__construct(UserFactory::ACCOUNT_TYPE_GUEST, $parameters);
    }

    public function GetUserName()
    {
        return 'Traveler';
    }

    protected function GetUniquePair()
    {
        throw new \BadMethodCallException('Not implemented exception!');
    }
}