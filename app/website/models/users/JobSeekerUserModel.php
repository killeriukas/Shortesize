<?php

namespace website\models\users;

use lib\data_structure\Dictionary;

final class JobSeekerUserModel extends UserModel
{

    /***
     * @var string
     */
    protected $firstName = '';

    /***
     * @var string
     */
    protected $lastName = '';

    public function __construct(Dictionary $parameters = null)
    {
       parent::__construct(UserFactory::ACCOUNT_TYPE_JOB_SEEKER, $parameters);
    }


    public function GetUserName()
    {
        return 'JS TEST NAME';
    }

    protected function GetUniquePair()
    {
        throw new \BadMethodCallException('Not implemented exception!');
    }
}