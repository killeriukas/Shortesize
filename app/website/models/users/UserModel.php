<?php

namespace website\models\users;

use lib\data_structure\Dictionary;
use lib\database\MongoDbHelper\MongoDbModel;
use lib\database\MongoDbHelper\MongoDbSingleton;
use lib\shared\Security;

abstract class UserModel extends MongoDbModel
{

    const USER_MODEL_ID = 'user_model_id';
    const USER_COLLECTION_ID = 'user_model';

    const UNIQUE_ID = 'user_id';

    /***
     * @var \MongoId
     */
    protected $uniqueId;

    /***
     * @var \MongoDate
     */
    protected $creationTime;

    /***
     * @var \MongoDate
     */
    protected $lastUpdateTime;

    protected $accountType;

    protected $email;

    protected $emailLowerCase;

    protected $password;

    protected $activated;

    protected function __construct($accountType, Dictionary $parameters = null)
    {
        parent::__construct(MongodbSingleton::GetInstance(), UserModel::USER_COLLECTION_ID, $parameters);
        $this->accountType = $accountType;
    }

    public function Save()
    {
        if (!empty($this->uniqueId)) {
            throw new \Exception("This user has been saved already. You are trying to overwrite it. Current ID: " . $this->uniqueId . ". Please UPDATE instead.");
        }

        $this->uniqueId = new \MongoId();
        $this->password = Security::CreateHash($this->password);
        $now = new \MongoDate();
        $this->creationTime = $now;
        $this->lastUpdateTime = $now;
        $this->emailLowerCase = mb_strtolower($this->email, ACCEPTED_ENCODING);
        $this->activated = false;

        parent::Save();
    }

    public abstract function GetUserName();

    public function GetUniqueId()
    {
        return $this->uniqueId;
    }

    public function GetEmail()
    {
        return $this->email;
    }

    public function GetAccountType()
    {
        return $this->accountType;
    }

    public function GetPassword()
    {
        return $this->password;
    }

}
