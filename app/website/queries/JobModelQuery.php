<?php

namespace website\queries;

use lib\data_structure\Dictionary;
use lib\database\Query;
use website\models\account\JobModel;

final class JobModelQuery extends Query
{

    public function CreateModel(Dictionary $data)
    {
        return new JobModel($data);
    }
}