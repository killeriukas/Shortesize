<?php

namespace website\controllers\auth\user;

use lib\data_structure\Dictionary;
use lib\database\MongoDbHelper\MongoDbSingleton;
use lib\shared\Security;
use lib\shared\Session;
use website\models\users\UserFactory;
use website\models\users\UserModel;

final class UserService
{

    /**
     * @var Session
     */
    private $session = null;

    /**
     * @var null|UserModel
     */
    private $user = null;

    public static function GetInstance()
    {
        static $instance = null;
        if (null === $instance) {
            $instance = new self();
        }
        return $instance;
    }

    private function __construct()
    {
        $this->session = Session::GetInstance();
    }

    public function SetUser(UserModel $user)
    {
        $this->user = $user;
    }

    /**
     * @param $password string
     * @return bool
     */
    public function LogIn($password)
    {
        $loggedIn = Security::ValidatePassword($password, $this->user->GetPassword());
        if ($loggedIn) {
            $this->session->Set(UserModel::USER_MODEL_ID, $this->user->GetUniqueId());
            return true;
        }

        return false;
    }

    public static function LoadUser(array $query)
    {
        $collection = MongoDbSingleton::GetInstance()->GetCollection(UserModel::USER_COLLECTION_ID);
        $data = $collection->find($query);

        $dataCount = $data->count();

        //user was not found
        if (0 === $dataCount) {
            return null;
        }

        if (1 < $dataCount) {
            throw new \Exception('Too many records found using the same query!');
        }

        //populate the class with the data from the database
        $recordInfo = $data->getNext();

        $user = UserFactory::CreateUserByType($recordInfo['accountType'], new Dictionary($recordInfo));
        return $user;
    }

    /**
     * @return bool
     */
    public static function IsLoggedIn()
    {
        $userId = self::GetLoggedInUserId();

        if (null !== $userId) {
            return true;
        }

        return false;
    }

    public static function GetLoggedInUserId()
    {
        return Session::GetInstance()->Get(UserModel::USER_MODEL_ID);
    }

    public function GetUser()
    {
        return $this->user;
    }

    /**
     * @return void
     */
    public static function LogOut()
    {
        Session::GetInstance()->Remove(UserModel::USER_MODEL_ID);
    }

}