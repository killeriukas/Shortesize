<?php

namespace website\controllers\auth;

use lib\html\form\BlankFormValidator;
use lib\html\form\FormValidator;
use lib\slim\AController;
use Slim\Slim;
use website\html\form\registration_form\RegistrationValidatorFactory;
use website\models\users\UserFactory;

final class RegisterController extends AController
{
    public function IndexAction()
    {
        $this->RenderRegistrationView($this->app, new BlankFormValidator());
    }

    function RenderRegistrationView(Slim $slimApp, FormValidator $formValidator)
    {
        $slimApp->render('/' . PROJECT_AUTH_FOLDER . '/RegisterView.php', array('formValidator' => $formValidator));
    }

    private function RegisterAccount(FormValidator $formValidator)
    {
        $accountType = $formValidator->GetEscaped('accountType');
        $newUser = UserFactory::CreateUserByType($accountType, $formValidator->GetData());
        $newUser->Save();
    }

    public function RegisterAction()
    {
        $formData = $this->app->request->post();

        $register = isset($formData['register']);
        $cancel = isset($formData['cancel']);

        if ($register) {
            $formValidator = RegistrationValidatorFactory::CreateValidator($formData['accountType'], $formData);

            if ($formValidator->IsValid()) {
                $this->RegisterAccount($formValidator);
                $this->app->redirectTo(WEB_LOGIN_GET);
            } else {
                $this->RenderRegistrationView($this->app, $formValidator);
            }
        } else if ($cancel) {
            $this->app->redirect('/');
        }
    }

}