<?php

namespace website\controllers\error;

use lib\slim\AController;

final class ErrorController extends AController
{

    public function AccessDeniedAction()
    {
        $this->app->render('/' . PROJECT_ERROR_FOLDER . '/AccessDeniedView.php');
    }

}