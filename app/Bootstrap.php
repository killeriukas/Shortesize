<?php

// Define the path to your root folder. 1 folder higher than this.
define('DEFAULT_ROOT', realpath(dirname(__FILE__) . '/..'));
define('DEFAULT_APP_ROOT', DEFAULT_ROOT . '/app');
define('DEFAULT_CONFIG', DEFAULT_APP_ROOT . '/config');
define('DEFAULT_WEBSITE_ROOT', DEFAULT_APP_ROOT . '/website');
define('DEFAULT_APP_VIEWS', DEFAULT_WEBSITE_ROOT . '/views');
define('DEFAULT_APP_CONTROLLERS', DEFAULT_WEBSITE_ROOT . '/controllers');
define('DEFAULT_APP_MODELS', DEFAULT_WEBSITE_ROOT . '/models');

define('PROJECT_MAIN_FOLDER', 'main');
define('PROJECT_AUTH_FOLDER', 'auth');
define('PROJECT_ACCOUNT_FOLDER', 'account');
define('PROJECT_ERROR_FOLDER', 'error');

require_once(DEFAULT_ROOT . '/vendor/autoload.php');

\lib\shared\Session::GetInstance()->StartSession();

$config = require DEFAULT_CONFIG . '/Config.php';

\lib\database\MongoDbHelper\MongoDbSingleton::GetInstance($config[APP_MODE]['db']);

$slimApp = new \Slim\Slim($config[APP_MODE]);
$slimApp->add(new \website\middleware\UserPermissionMiddleware());
$slimApp->add(new \website\middleware\UserLoginMiddleware());

$allRoutes = require DEFAULT_CONFIG . '/Routes.php';

foreach ($allRoutes as $routeName => $routeParam) {
    $slimApp->{$routeParam['request_type']}($routeParam['url'], $routeParam['controller'])->setName($routeName);
}

$slimApp->run();